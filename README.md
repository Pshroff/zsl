﻿# Zero Shot Learning Based Object Classification 

This respository contains the code and dataset for the course project.  


# Prerequisite

*Pytorchv1.0+  
*Numpy  
*torchfile - from - [https://github.com/bshillingford/python-torchfile](https://github.com/bshillingford/python-torchfile)  

##  RUN  

To run specific "semantic " with specific "metric" use following convention and Run corresponding file.  

-attribute with MSE loss:  python att_mse.py   
-description with MSE loss:  python desc_mse.py  
-fusion with MSE loss:  python fusion_mse.py  
-fusion with cosine loss:  python fusion_cosine.py  
-fusion with mahalanobis loss:  python fusion_mah.py  

## Reference  

[1] S. Reed, Z. Akata, B. Schiele, and H. Lee. Learning deep
representations of fine-grained visual descriptions.   
[2] L. Zhang, T. Xiang, and S. Gong, “Learning a deep embedding model for zero-shot learning.  


